#ifndef _REST_SL_MODULE
#define _REST_SL_MODULE

#include "../../framework/service_locator/ServiceLocator.hpp"
#include "../../framework/restinio/RestinioController.hpp"

#include "DemoRest.hpp"

using namespace framework::restinio;

namespace business::rest
{

    class RestSLModule : public ServiceLocator::Module
    {
    public:
        void load() override;
    };

    void RestSLModule::load()
    {

        // Demo Rest
        bind<RestinioController>("demoRest").to<DemoRest>(
            [&](SLContext_sptr slc) {
            return new DemoRest();
            }
        ).asSingleton();
    }

}

#endif