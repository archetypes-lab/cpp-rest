#ifndef _DEMO_REST
#define _DEMO_REST

#include "../../framework/restinio/RestinioController.hpp"

using namespace framework::restinio;

namespace business::rest
{

    class DemoRest : public RestinioController
    {
    private:
        /* data */
    public:
        DemoRest(/* args */);
        ~DemoRest() override;
        void requestHandler(std::unique_ptr<router_t> &router) override;
    };

    DemoRest::DemoRest(/* args */)
    {
    }

    DemoRest::~DemoRest()
    {
    }

    void DemoRest::requestHandler(std::unique_ptr<router_t> &router)
    {
        router->http_get(
            "/",
            [](auto req, auto params) {
                return req->create_response()
                        .append_header(restinio::http_field::content_type, "text/plain; charset=utf-8")
                        .set_body("Hello world!")
                        .done();
            });

        router->http_get(
            "/json",
            [](auto req, auto params) {
                return req->create_response()
                    .append_header(restinio::http_field::content_type, "text/json; charset=utf-8")
                    .set_body(R"-({"message" : "Hello world!"})-")
                    .done();
            });

        router->http_get(
            "/html",
            [](auto req, auto params) {
                return req->create_response()
                    .append_header(restinio::http_field::content_type, "text/html; charset=utf-8")
                    .set_body(
                        "<html>\r\n"
                        "  <head><title>Hello from RESTinio!</title></head>\r\n"
                        "  <body>\r\n"
                        "    <center><h1>Hello</h1></center>\r\n"
                        "  </body>\r\n"
                        "</html>\r\n")
                    .done();
            });
    }

}

#endif