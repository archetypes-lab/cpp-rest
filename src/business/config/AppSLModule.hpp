#ifndef _APP_CONFIG_MODULE
#define _APP_CONFIG_MODULE

#include <cstdlib>
#include <string>
#include <regex>

#include "../../framework/service_locator/ServiceLocator.hpp"
#include "../../framework/app_config/AppConfig.hpp"
#include "../../framework/file_properties/IFilePropertiesUtils.hpp"
#include "../../framework/file_properties/YamlFilePropertiesUtils.hpp"

using namespace framework::app_config;
using namespace framework::file_properties;

namespace business::config {

    class AppSLModule : public ServiceLocator::Module
    {
    public:
        void load() override;

    private:
        static const std::string APPCONFIG_FILE;
    };

    const string AppSLModule::APPCONFIG_FILE = "./resources/appconfig-PROFILE.yaml";

    void AppSLModule::load()
    {

        // Properties Utils
        bind<IFilePropertiesUtils>("yamlFilePropertiesUtils").to<YamlFilePropertiesUtils>([&](SLContext_sptr slc) {
            try {
                // Obtener profile desde enviroment
                char *appProfileEnv = getenv("APP_PROFILE");
                string appProfile = appProfileEnv != NULL ? appProfileEnv : "default";

                // Config File desde Profile
                string configFilePath = regex_replace(APPCONFIG_FILE, regex("PROFILE"), appProfile);

                return new YamlFilePropertiesUtils(configFilePath);
            } catch(const std::exception & ex) {
                std::cerr << ex.what() << std::endl;
                throw;
            }
        }).asSingleton();

        // Config Utils
        bind<AppConfig>("appConfig").to<AppConfig>([&](SLContext_sptr slc) {
            try {
                return new AppConfig(slc->resolve<IFilePropertiesUtils>("yamlFilePropertiesUtils"));
            } catch(const std::exception & ex) {
                std::cerr << ex.what() << std::endl;
                throw;
            }
        }).asSingleton();
    }

}

#endif