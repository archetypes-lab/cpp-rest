#ifndef _ENTITY_USUARIO
#define _ENTITY_USUARIO

#include <string>

namespace business::entities
{

    class Usuario
    {

    private:
        long idUsuario;
        std::string username;
        std::string nombres;
        std::string apellidos;
        std::string email;

    public:
        void setIdUsuario(long idUsuario);
        long getIdUsuario();
        void setUsername(std::string username);
        std::string getUsername();
        void setNombres(std::string nombres);
        std::string getNombres();
        void setApellidos(std::string apellidos);
        std::string getApellidos();
        void setEmail(std::string email);
        std::string getEmail();
    };

    void Usuario::setIdUsuario(long idUsuario)
    {
        this->idUsuario = idUsuario;
    }

    long Usuario::getIdUsuario()
    {
        return this->idUsuario;
    }

    void Usuario::setUsername(std::string username)
    {
        this->username = username;
    }

    std::string Usuario::getUsername()
    {
        return this->username;
    }

    void Usuario::setNombres(std::string nombres)
    {
        this->nombres = nombres;
    }

    std::string Usuario::getNombres()
    {
        return this->nombres;
    }

    void Usuario::setApellidos(std::string apellidos)
    {
        this->apellidos = apellidos;
    }

    std::string Usuario::getApellidos()
    {
        return this->apellidos;
    }

    void Usuario::setEmail(std::string email)
    {
        this->email = email;
    }

    std::string Usuario::getEmail()
    {
        return this->email;
    }

}

#endif