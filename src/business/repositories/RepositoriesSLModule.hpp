#ifndef _REPOSITORIES_SL_MODULE
#define _REPOSITORIES_SL_MODULE

#include "../../framework/service_locator/ServiceLocator.hpp"

namespace business::repositories
{

    class RepositoriesSLModule : public ServiceLocator::Module
    {
        public:
            void load() override;
    };

    void RepositoriesSLModule::load() {

        //Mysql Connection

    }

}

#endif