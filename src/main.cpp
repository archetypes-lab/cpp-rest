#include <iostream>
#include "framework/service_locator/ServiceLocatorUtils.hpp"
#include "business/config/AppSLModule.hpp"
#include "business/rest/RestSLModule.hpp"

using namespace framework::restinio;
using namespace framework::service_locator;

using namespace business;

using router_t = restinio::router::express_router_t<>;

int main()
{

  // Load Service Locator Modules

  ServiceLocatorUtils::addModule<config::AppSLModule>();
  ServiceLocatorUtils::addModule<rest::RestSLModule>();

  try
  {

    // Config Restinio Controllers

    auto router = std::make_unique<router_t>();

    std::vector<std::shared_ptr<RestinioController>> controllers;
    ServiceLocatorUtils::resolveAll(&controllers);

    for (auto controller : controllers)
    {
      controller->requestHandler(router);
    }

    // restinio

    using namespace std::chrono;

    using traits_t =
        restinio::traits_t<
            restinio::asio_timer_manager_t,
            restinio::shared_ostream_logger_t,
            router_t>;

    restinio::run(
        restinio::on_thread_pool<traits_t>(8)
            .address("0.0.0.0")
            .port(5050)
            .request_handler(std::move(router))
            .read_next_http_message_timelimit(10s)
            .write_http_response_timelimit(1s)
            .handle_request_timeout(1s));
  }
  catch (const std::exception &ex)
  {
    std::cerr << "Error: " << ex.what() << std::endl;
    return 1;
  }

  return 0;
}
