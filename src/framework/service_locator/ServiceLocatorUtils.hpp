#ifndef _SERVICE_LOCATOR_UTILS_
#define _SERVICE_LOCATOR_UTILS_

#include "ServiceLocator.hpp"

using namespace std;

namespace framework::service_locator
{

    class ServiceLocatorUtils
    {

    public:
        template <class T>
        static void addModule();

        template <class T>
        static shared_ptr<T> resolve(const string &named);

        template <class T>
        static shared_ptr<T> resolve();

        template <class T>
        static void resolveAll(vector<shared_ptr<T>> *all);

    private:
        static shared_ptr<ServiceLocator> instance;

        static shared_ptr<ServiceLocator> getInstance();
    };

    shared_ptr<ServiceLocator> ServiceLocatorUtils::instance = nullptr;

    template <class T>
    void ServiceLocatorUtils::addModule()
    {
        auto sl = getInstance();
        sl->modules().add<T>();
    }

    template <class T>
    shared_ptr<T> ServiceLocatorUtils::resolve(const string &named)
    {
        auto slc = getInstance()->getContext();
        return slc->resolve<T>(named);
    }

    template <class T>
    shared_ptr<T> ServiceLocatorUtils::resolve()
    {
        auto slc = getInstance()->getContext();
        return slc->resolve<T>();
    }

    template <class T>
    void ServiceLocatorUtils::resolveAll(std::vector<shared_ptr<T>> *all)
    {
        auto slc = getInstance()->getContext();
        slc->resolveAll(all);
    }

    shared_ptr<ServiceLocator> ServiceLocatorUtils::getInstance()
    {
        if (instance == nullptr)
        {
            instance = ServiceLocator::create();
        }
        return instance;
    }

}

#endif