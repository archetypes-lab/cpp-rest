#ifndef _APP_CONFIG
#define _APP_CONFIG

#include <string>
#include <memory>
#include <cstdlib>

#include "../file_properties/IFilePropertiesUtils.hpp"

using namespace std;
using namespace framework::file_properties;

namespace framework::app_config
{
    class AppConfig
    {
    public:
        AppConfig(const shared_ptr<IFilePropertiesUtils> &propertiesUtils);
        ~AppConfig();
        const string getProperty(const string &name);

    protected:
        shared_ptr<IFilePropertiesUtils> propertiesUtils;
    };

    AppConfig::AppConfig(const shared_ptr<IFilePropertiesUtils> &propertiesUtils)
        : propertiesUtils(propertiesUtils) {}

    AppConfig::~AppConfig() {}

    const string AppConfig::getProperty(const string &nombrePropiedad)
    {
        if (nombrePropiedad.empty())
        {
            return "";
        }

        char *envValue = getenv(nombrePropiedad.c_str());
        return envValue != NULL
                   ? envValue
                   : this->propertiesUtils->getProperty(nombrePropiedad);
    }

}

#endif