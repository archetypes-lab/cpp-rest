#ifndef _RESTINIO_CONTROLLER
#define _RESTINIO_CONTROLLER

#include <restinio/all.hpp>

using router_t = restinio::router::express_router_t<>;

namespace framework::restinio
{

    class RestinioController
    {
        public:
            virtual ~RestinioController(){};
            virtual void requestHandler(std::unique_ptr<router_t> & router) = 0;
    };

}

#endif