#ifndef _IFILEPROPERTIESUTILS
#define _IFILEPROPERTIESUTILS

#include <string>

namespace framework::file_properties
{
    class IFilePropertiesUtils
    {
    public:
        virtual ~IFilePropertiesUtils(){};
        virtual const std::string getProperty(const std::string &name) = 0;
    };
}

#endif