#ifndef _YAML_FILE_PROPERTIES_UTILS
#define _YAML_FILE_PROPERTIES_UTILS

#include "IFilePropertiesUtils.hpp"
#include <yaml-cpp/yaml.h>

namespace framework::file_properties
{
    /**
     * yaml parser: https://github.com/jbeder/yaml-cpp/wiki/Tutorial
     * */
    class YamlFilePropertiesUtils : public IFilePropertiesUtils
    {

    public:
        YamlFilePropertiesUtils(const std::string &filename);
        ~YamlFilePropertiesUtils() override;

        const std::string getProperty(const std::string &propertyName) override;

    private:
        std::string filename;
        YAML::Node yamlNode;
    };

    YamlFilePropertiesUtils::YamlFilePropertiesUtils(const string &filename)
        : filename(filename)
    {
        this->yamlNode = YAML::LoadFile(filename);
    }

    YamlFilePropertiesUtils::~YamlFilePropertiesUtils() {}

    const std::string YamlFilePropertiesUtils::getProperty(const std::string &propertyName)
    {
        return this->yamlNode[propertyName] ? this->yamlNode[propertyName].as<std::string>() : "";
    }

}

#endif