FROM debian:9-slim

EXPOSE 5050

WORKDIR /root/build/bin
COPY build/bin .
COPY resources ./resources

CMD ./main
